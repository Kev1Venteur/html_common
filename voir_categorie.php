<?php $title="Voir Categorie" ?>
<?php include_once "header.php" ?>
<?php
$id= filter_input(INPUT_GET, "id");
require_once 'Config.php';

$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE
        , Config::USER, Config::PASSWORD);
$r = $db->prepare("select id, titre, description from categories where id=:id");

$r->bindparam(":id",$id);

$r->execute();

$categorie = $r->fetch();
?>
<h1><?php echo $categorie["titre"]?></h1>
<h3><?php echo $categorie["description"] ?></h3>
<?php include_once "voir_chaton.php" ?>
<a href="ajouter_chaton.php" class="btn-large">
    <i class="material-icons left">add_circle_outline</i>
    Ajouter un châton...
</a>
<?php include_once "footer.php" ?>
