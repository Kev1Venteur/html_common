<?php $nomDuDossier=$_GET["dossier"];?>
<?php $title="Dossier $nomDuDossier - dossier" ?>
<?php include_once "header.php"?>
<h1>Test liste
  <?php echo $nomDuDossier?>
</h1>
<div class="row">
<?php $images=scandir(__DIR__."/photo/".$nomDuDossier);
foreach ($images as $value){
  if ($value != "." && $value != "..") {
  ?>
    <div class="col s12 m6">
      <div class="card">
        <div class="card-image">
          <img src="/dossier/photo/<?php echo "$nomDuDossier/$value"; ?>">
        </div>
      </div>
    </div>

  <?php
}
} ?>
</div>
<form action="ajouterPhoto.php" method="post" enctype="multipart/form-data">
  <label for="image">Ajouter une photo</label>
  <input type="file" name="image" id="image">
  <input type="hidden" value="<?php echo $nomDuDossier?>" name="dossier">
  <input type="submit" value="OK">
</form>
<?php include_once "footer.php"?>
