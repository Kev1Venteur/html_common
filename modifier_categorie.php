<?php
$id= filter_input(INPUT_GET, "id");
require_once 'Config.php';

$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE
        , Config::USER, Config::PASSWORD);
$r = $db->prepare("select id, titre, description from categories where id=:id");

$r->bindparam(":id",$id);

$r->execute();

$categorie = $r->fetch();
?>

<?php $title="Modifier la categorie ".$categorie["titre"]." - Chatons Mignons" ?>
<?php include_once "header.php" ?>
<h1>Modifier une categorie <?php echo $categorie["titre"] ?></h1>
<form action="action/modifierUneCategorie.php" method="post">
  <div class="row">
    <div class="input-field col s12">
      <input type="text" name="titre" id="titre" class="validate"
              maxlength="50" required value="<?php echo htmlspecialchars($categorie['titre']) ?>  ">
      <label for="titre">Titre</label>
    </div>
    <div class="input-field col s12">
      <textarea id="description" name="description"
          class="materialize-textarea"><?php echo $categorie["description"] ?></textarea>
      <label for="description">Description</label>
    </div>
    <div class="input-field col s12">
      <a href="index.php" class="btn red left">Annuler</a>
      <input type="submit" value="OK" class="btn-large right">
    </div>
  </div>
  <input type="hidden" name="id" value="<?php echo $id ?>">
</form>
<?php include_once "footer.php" ?>
