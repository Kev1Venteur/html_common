<?php $title = "Accueil - Chatons Mignons" ?>
<?php include_once "header.php" ?>

<!--ici on mettra le contenu des pages-->
<h1>Chatons mignons</h1>

<?php
require_once 'Config.php';

$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE
        , Config::USER, Config::PASSWORD);
$r = $db->prepare("select id, titre, description from categories");

$r->execute();

//récupération du résultat du select
$lignes = $r->fetchAll(); //retourne un tableau
?>
<table>
    <thead>
        <tr>
            <th>Titre</th>
            <th>Voir</th>
            <th>Actions</th>
        </tr>
    </thead>

    <tbody>
<?php
//attention au pluriel/singulier :)
foreach ($lignes as $ligne) {
    ?>
            <tr>
                <td><?php echo $ligne["titre"] ?></td>
                <td>
                    <a href="voir_categorie.php?id=<?php echo $ligne["id"] ?>"
                       class="btn blue">
                        Voir <i class="material-icons right">photo_camera</i>
                    </a>
                </td>
                <td>
                    <a href="modifier_categorie.php?id=<?php echo $ligne["id"] ?>"
                       class="btn-floating">
                        <i class="material-icons">edit</i>
                    </a>
                    <a href="action/supprimerUneCategorie.php?id=<?php echo $ligne["id"] ?>"
                       class="btn-floating red">
                        <i class="material-icons">delete</i>
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>

<a href="ajouter_categorie.php" class="btn-large">
    <i class="material-icons left">add_circle_outline</i>
    Ajouter une catégorie...
</a>

<?php include_once "footer.php" ?>
