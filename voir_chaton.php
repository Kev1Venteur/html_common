<?php
$id= filter_input(INPUT_GET, "id");
require_once 'Config.php';

$db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE
        , Config::USER, Config::PASSWORD);
$r = $db->prepare("select id, nom, description, photo from chatons where id=:id");

$r->bindparam(":id",$id);

$r->execute();

$chatons = $r->fetch();
?>
<h1><?php echo $chatons["nom"]?></h1>
<h3><?php echo $chatons["description"] ?></h3>
<?php echo $chatons["photo"]?>
