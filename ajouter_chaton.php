<?php $title="Ajouter un Chaton" ?>
<?php include_once "header.php" ?>

<h1>Ajouter un chaton</h1>
<form action="action/ajouterUnChaton.php" method="post">
  <div class="row">

    <div class="input-field col s12">
      <input type="text" name="nom" id="nom" class="validate"
              maxlength="50" required>
      <label for="titre">Nom_du_chaton</label>
    </div>

    <div class="input-field col s12">
      <textarea id="description" name="description"
          class="materialize-textarea"></textarea>
      <label for="description">Description</label>
    </div>

    <div class="input-field col s12">
      <textarea id="photo" name="photo"
          class="materialize-textarea"></textarea>
      <label for="photo">Photo</label>
    </div>

      <div class="input-field col s12">
          <select name="idCategorie">
            <option value="" disabled selected>Choisissez votre categorie</option>
            <?php require_once 'Config.php';

            $db = new PDO("mysql:host=" . Config::SERVEUR . ";dbname=" . Config::BASE
                    , Config::USER, Config::PASSWORD);
            $r = $db->prepare("select id, titre from categories");

            $r->execute();

            $categories = $r->fetchAll();
            ?>
            <?php foreach ($categories as $categorie){
              echo '<option value="'.$categorie["id"].'">'.htmlspecialchars($categorie["titre"]).'</option>';
            }

            ?>

          </select>
        <label>Materialize Select</label>
      </div>

    <div class="input-field col s12">
      <a href="index.php" class="btn red left">Annuler</a>
      <input type="submit" value="OK" class="btn-large right">
    </div>
  </div>
</form>
<?php include_once "footer.php" ?>
