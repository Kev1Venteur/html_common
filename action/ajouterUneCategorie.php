<?php
$titre = filter_input(INPUT_POST, "titre");
$description = filter_input(INPUT_POST, "description");

require_once '../Config.php';
$db=new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::USER, Config::PASSWORD);
$r = $db->prepare("insert into categories (titre, description)"
    ." values (:titre,:description)");
//Préparation de la requête
    
$r->bindParam(":titre", $titre);
$r->bindParam(":description", $description);

$r->execute();
header('Location: ../index.php');
?>
