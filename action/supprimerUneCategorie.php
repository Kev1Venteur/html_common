<?php
$id = filter_input(INPUT_GET, "id");

require_once '../Config.php';
$db=new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::USER, Config::PASSWORD);
$r = $db->prepare("delete from categories where id=:id");

$r->bindParam(":id", $id);

$r->execute();
header('Location: ../index.php');
?>
