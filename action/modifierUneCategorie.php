<?php
$titre = filter_input(INPUT_POST, "titre");
$description = filter_input(INPUT_POST, "description");
$id = filter_input(INPUT_POST, "id");

require_once '../Config.php';
$db=new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::USER, Config::PASSWORD);
$r = $db->prepare("update categories set titre=:titre, description=:description"
    ." where id=:id");
//Préparation de la requête

$r->bindParam(":titre", $titre);
$r->bindParam(":description", $description);
$r->bindParam(":id", $id);

$r->execute();
header('Location: ../index.php');
?>
