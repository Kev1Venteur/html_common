<?php
$nom = filter_input(INPUT_POST, "nom");
$description = filter_input(INPUT_POST, "description");
$photo = filter_input(INPUT_POST, "photo");
$idCategorie = filter_input(INPUT_POST, "idCategorie");

//  echo "$nom $description $photo $idCategorie";

require_once '../Config.php';
$db=new PDO("mysql:host=".Config::SERVEUR.";dbname=".Config::BASE, Config::USER, Config::PASSWORD);
$r = $db->prepare("insert into chatons (nom, description, photo, idCategorie)"
    ." values (:nom, :description, :photo, :idCategorie)");
//Préparation de la requête

$r->bindParam(":nom", $nom);
$r->bindParam(":description", $description);
$r->bindParam(":photo", $photo);
$r->bindParam(":idCategorie", $idCategorie);

$r->execute();
header('Location: ../index.php');
?>
